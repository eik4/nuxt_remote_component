package main

import (
  // "fmt"
  // "log"
  "time"
  "io/ioutil"
  // "encoding/json"
  // "strings"
  // "strconv"
  // "reflect"
  // "errors"
  // "dynitf/rest/itfmake"
  // "dynitf/rest/dbaccess"

  "github.com/gin-contrib/cors"
  "github.com/gin-gonic/gin"
)

func main() {

  r := gin.Default()

  r.Use(cors.New(cors.Config{
    AllowOrigins:     []string{"*"},
    AllowMethods:     []string{"GET", "PUT", "POST", "DELETE"},
    AllowHeaders:     []string{"Origin", "Authorization", "Content-Type", "Content-Length", "Accept", "Accept-Encoding", "X-HttpRequest"},
    ExposeHeaders:    []string{"Content-Length"},
    AllowCredentials: true,
    AllowOriginFunc: func(origin string) bool {
      return true
    },
    MaxAge: 12 * time.Hour,
  }))

  // get component
  r.GET("/component", func(c *gin.Context) {

    file, _ := ioutil.ReadFile("interface/component.js")

    c.JSON(200, gin.H{
      "message": string(file),
    })
  })

  r.Run(":4000")
}
